
'use strict';

const { 
  app, 
  BrowserWindow 
} = require('electron')

//app.commandLine.appendSwitch('disable-gpu');

//SET_IP_PORT_START
var IP = '127.0.0.1';
var PORT = 8000;
//SET_IP_PORT_END 

app.on('ready', () => {
	
	var overlay = {};

	function lance_overlay(name, x, y, w, h, click_through) {

		overlay[name] = new BrowserWindow({
			frame: false,
			transparent: true,
			webPreferences: {
				nodeIntegration: false   
			},
			resizable: true,
			x: x,
			y: y,
			width: w,
			height: h,
			alwaysOnTop: true
		});
		overlay[name].loadURL('http://'+ IP + ':' + PORT +'/' + name + '.html');

		//overlay[name].webContents.openDevTools()

		overlay[name].on('focus', function() {
			overlay[name].setResizable(true)
		})
		overlay[name].on('blur', function() {
			overlay[name].setResizable(false)
		})
		if (click_through == 1)
			overlay[name].setIgnoreMouseEvents(true);
	}
	
	//SET_OVERLAYS_START
	lance_overlay("compteur", 0,0,300,300, 0);
	//SET_OVERLAYS_END
	
	var overlay_perso = {};
	overlay_perso = new BrowserWindow({
		frame: false,
		transparent: true,
		webPreferences: {
			nodeIntegration: false   
		},
		resizable: true,
		x: 3840,
		y: 1060,
		width: 243,
		height: 140,
		alwaysOnTop: true
	});
	overlay_perso.loadURL('http://127.0.0.1:8182/tyres-temp/overlay.html#?fps=1&carTemps=false&showTyres=true&carFuel=false');
	
});

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});

